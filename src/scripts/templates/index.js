import {load} from '@shopify/theme-sections';
import '../sections/featured-collection';
import {flickity} from 'flickity';

document.addEventListener('DOMContentLoaded', () => {
  load('*');
});
